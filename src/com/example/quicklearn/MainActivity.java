package com.example.quicklearn;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.view.Menu;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends Activity {
	
	ListView videolist;

	ArrayList<String> videoArrayList = new ArrayList<String>();

	ArrayAdapter<String> videoadapter;
	Context context;

	String feedURL = "https://gdata.youtube.com/feeds/api/users/StevesLectures/uploads?v=2&alt=jsonc&start-index=1&max-results=15";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    context = this;
	   setContentView(R.layout.activity_main);
	    videolist = (ListView) findViewById(R.id.videolist);

	    videoadapter = new ArrayAdapter<String>(this, R.layout.video_list_itrm,
	            videoArrayList);

	    videolist.setAdapter(videoadapter);
	    VideoListTask loadertask = new VideoListTask();
	    loadertask.execute();
	}

	private class VideoListTask extends AsyncTask<Void, String, Void> {

	    ProgressDialog dialogue;

	    @Override
	    protected void onPostExecute(Void result) {
	        super.onPostExecute(result);
	        dialogue.dismiss();

	        videoadapter.notifyDataSetChanged();
	    }

	    @Override
	    protected void onPreExecute() {
	        dialogue = new ProgressDialog(context);

	        dialogue.setTitle("Loading items..");
	        dialogue.show();
	        super.onPreExecute();

	    }

	    @Override
	    protected Void doInBackground(Void... params) {

	        HttpClient client = new DefaultHttpClient();
	        HttpGet getRequest = new HttpGet(feedURL);
	        try {
	            HttpResponse response = client.execute(getRequest);
	            StatusLine statusline = response.getStatusLine();
	            int statuscode = statusline.getStatusCode();

	            if (statuscode != 200) {

	                return null;
	            }

	            InputStream jsonStream = response.getEntity().getContent();

	            BufferedReader reader = new BufferedReader(
	                    new InputStreamReader(jsonStream));

	            StringBuilder builder = new StringBuilder();
	            String line;

	            while ((line = reader.readLine()) != null) {

	                builder.append(line);

	            }
	            String jsonData = builder.toString();

	            JSONObject json = new JSONObject(jsonData);

	            JSONObject data = json.getJSONObject("data");

	            JSONArray items = data.getJSONArray("items");

	            for (int i = 0; i < items.length(); i++) {

	                JSONObject video = items.getJSONObject(i);

	                videoArrayList.add(video.getString("title"));

	            }

	            Log.i("YouJsonData:", jsonData);

	        } catch (ClientProtocolException e) {

	            e.printStackTrace();
	        } catch (IOException e) {

	            e.printStackTrace();
	        } catch (JSONException e) {

	            e.printStackTrace();
	        }

	        return null;
	    }
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    getMenuInflater().inflate(R.menu.main, menu);
	    return true;
	}
	
	
}
